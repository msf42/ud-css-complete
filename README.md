# CSS - The Complete Guide

I've already used CSS quite a bit, and I use it daily at work. However, I wanted to get practice using some of the parts of CSS that I don't use on a daily basis at my job. Maximilian Schwarzmüller's courses are excellent. I've already taken several from him. I thought his course would be a great chance to sharpen my CSS skills.

---
