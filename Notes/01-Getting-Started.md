# Getting Started

## 001 Intro

- done

---

## 002: What is CSS?

- just a review

---

## 003: Join Our Online Learning Community

[discord](https://discord.gg/gxvEWGU)

---

## 004: CSS History, Present, & Future

- CSS1 (1996)
- CSS2 (1998)
- CSS3 (current development)

There will be no CSS4. CSS3 will stay in development, and focus will be on different modules.

---

## 005: Course Outline

![image](images/Lesson005.png)

There are 3 tracks - Basic, Advanced, Expert

- I think I will start with basic track, even though I have used it quite a bit
- I'll move quickly through the early modules, and fill in the gaps

---

## 006: Choose Your Track

- done

---

## 007: Course Prerequisites

- no worries

---

## 008: How to Get the Most Out of the Course

- done

---

## 009: Recommended Tools

- VSCode and browser, that's it!

---

## 010: Where to Find the Source Code

- attached to lectures

---

## 011: Useful Resources & Links

- Done

---
