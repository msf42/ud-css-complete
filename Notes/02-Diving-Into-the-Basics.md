# Diving Into the Basics of CSS

- this section is review for me, so I did not take many notes

## 012: Module Introduction

- how to add CSS to HTML
- setting up CSS rules
- selectors, properties, values
- conflicting styles

---

## 013: Understanding the Course Project Setup

- simple setup

---

## 014: Adding CSS to our Project with Inline Styles

- just an example of inline styles

---

## 015: Understanding the `<style>` Tag and Creating a .css File

- intro to using style tags

![image](images/Lesson015.png)

---

## 016: Applying Additional Styles & Importing Google Fonts

[Google Fonts](https://fonts.google.com/)

---

## 017: Theory Time - Selectors

![image](images/Lesson017a.png)

![image](images/Lesson017b.png)

---

## 018: Understanding the "Cascading" Style & Specificity

![image](images/Lesson018a.png)

![image](images/Lesson018b.png)

---

## 019: Understanding Inheritance

![image](images/Lesson019.png)

---

## 020: Adding Combinators

```css
/*
applies to all h1 elements inside element
with product-overview id
*/
#product-overview h1 {
  color: white;
  font-family: 'Anton', sans-serif;
}
```

---

## 021: Theory Time - Combinators

![image](images/Lesson021a.png)

![image](images/Lesson021b.png)

![image](images/Lesson021c.png)

![image](images/Lesson021d.png)

![image](images/Lesson021e.png)

---

## 022: Summarizing Properties & Selectors

![image](images/Lesson022.png)

---

## Quiz 1: Selectors & Combinators

- done

---

## Assignment 1: The Basics

```css
body {
  font-family: Arial, Helvetica, sans-serif;
}

h1 {
  color: blue;
  font-style: sans-serif;
}

code {
  font-family: monospace;
  color: red;
}

#green-code code {
  color: green;
}

li + li {
  background-color: black;
  color: white;
}

ol > li {
  background-color: black;
  color: white;
}
```

---

## 023: Wrap Up

![image](images/Lesson023.png)

---

## 024: Useful Resources & Links

- just some MDN refs

---
