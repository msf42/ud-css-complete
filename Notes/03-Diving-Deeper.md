# Diving Deeper into CSS (Working with Boxes)

## 025: Module Introduction

- the Box Model
- Height & Width
- the `display` property
- properties worth remembering
- pseudo classes & elements

---

## 026: Introducing the CSS Box Model

---

## 027: Understanding the Box Model

---

## 028: Understanding Margin Collapsing and Removing Default Margins

---

## 029: Deep Dive on "Margin Collapsing"

---

## 030: Theory Time - Working with Shorthand Properties

---

## 031: Applying Shorthands in Practice

---

## 032: Diving Into Height and Width Properties

---

## 033: Understanding Box Sizing

---

## 034: Adding the Header to Our Project

---

## 035: Understanding the Display Property

---

## 036: `display: none;` vs `visibility: hidden;`

---

## 037: HTML Refresher

---

## 038: Applying the Display Property & Styling Our Navigation Bar

---

## 039: Understanding an Unexpected `inline-block` Behavior

---

## 040: Working with `text-decoration` and `vertical-align`

---

## 041: Styling Anchor Tags

---

## 042: Adding Pseudo Classes

---

## 043: Theory Time - Pseudo Classes & Pseudo Elements

---

## 044: Grouping Rules

---

## 045: Working with `font-weight` and `border`

---

## 046: Adding & Styling a CTA-Button

---

## 047: Adding a Background Image to Our Project

---

## 048: Properties Worth Remembering

---

## Assignment 2: Diving Deeper Into CSS

---

## 049: Wrap Up

---

## 050: Useful Resources & Links

---
